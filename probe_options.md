# probe configuration options
probe has config in json format

minimal configuration for first start:
```json
{
  "listen": "0.0.0.0:12345",
  "source": "0.0.0.0",
  "aesKey": "00000000000000000000000000000000"
}
```

## main options

* `listen` ip:port to listen for connection from master instance
* `source` IPv4 ip to use as a source for all IPv6 ping, traceroute and http(s) tests, default value `0.0.0.0` means using default network interface
* `source6` same as source, but for IPv6
* `aesKey` is 16-bytes HEX representation of key used to encrypt comunication between main server and probes
* `sendQueueSize` default value 900 allowing up to 15 minutes of test data can be stored in memory in case of communication issues with main server
* `ping` key map, described below
* `trace` key map, described below

### ping options

* `preventConcurrent` if `true` doesn't allow start next ping cyrcle until previous completed
* `packetSize` icmp packet *payload* size
* `ttl` TTL for icmp packets
* `dontdefrag` if `true` do not defragment flag is set for icmp packets
* `IdStart` minimal icmp Id to use (or PID in case of 0)
* `IdEnd` maximum icmp Id to use
* `IdRandom` use random Id between `IdStart` and `IdEnd` (`IdStart` have to be > 0)
* `IdPidStart` if true and `IdStart` have to be > 0 both `IdStart` and `IdEnd` will be increased by PID (for example if process Id is 1539 and `IdStart` set to 1 and `IdEnd` set to 100 ICMP Ids from 1540 to 1640 will be used)
* `SeqStart` same as `IdStart` for ICMP seq
* `SeqEnd` same as `IdEnd` for ICMP seq
* `SeqRandom` same as `IdRandom` for ICMP seq

### trace options 

* `rounds` how much packets with same destination and ttl to send
* `workers` to how many hosts send at once
* `maxhops` maximum ttl (to high value result is much more packets sent)
* `delay` delay between traceroute chunks
* `type` either `builtin` or `tool`, in case of `tool` external command used to make traceroute and format it to json format
* `toolcmd` command to make traceroute and output in right json format, default value `mtr -i $interval -z $timeout -m $maxhops -c $count -n -z -j $ip | jq '[ .report.hubs[] | [ { ip: .host, as: .ASN | ltrimstr("AS") | tonumber, min: .Best | tostring, max: .Wrst | tostring, avg: .Avg | tostring, received: (100- (.["Loss%"]))  }  ] ]'`
* `IdStart` same as `ping.IdStart` for traceroutes
* `IdEnd` same as `ping.IdEnd` for traceroutes
* `IdRandom` same as `ping.IdRandom` for traceroutes
* `IdPidStart` same as `ping.IdPidStart` for traceroutes
